db.fruits.insertMany([
    {
        name: "apple",
        color: "red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: [ "Philippines","US" ]
    },
    {
        name: "banana",
        color: "yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: [ "Philippines" , "Ecuador" ]
    },
    {
        name: "kiwi",
        color: "green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: [ "US" , "China" ]
    },
    {
        name:"mango",
        color: "green",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: [ "Philippines" , "India" ]
    }
]);

// SECTION - mongoDB Aggregation
/**
 * -used to generate manipulated data and perform operations to create filtered results that help in analyzing data
 * -compared CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with 
 * information to make necessary developement decisions without having to create a frontend application
 */

// SECTION - USING AGGREGATE METHOD
/**
 * $match
 *  -used to pass the documents that meet the specified conditions(s) to the pipeline/aggregation
 * process
 * SYNTAX:
 * db.collectionName.aggregate([
 *  {
 *      $match:{
 *          field: value
 *      }
 *  }
 * ]);
 */
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    }
]);

/**
 * $group
 *  -used to group elements together and field-value pairs using the data from the grouped elements
 * SYNTAX:
 * db.collectionName.aggregate([
 *  {
 *      $group:{
 *          field: "$value",
 *          field: {
 *              $field: "$value"
 *          }
 *      }
 *  }
 * ]);
 */
db.fruits.aggregate([
    {
        $group:{
            _id:"$supplier_id",
            total:{
                $sum: "$stock"
            }
        }
    }
]);

/**
 * Combination of $match and $group
 */
 db.fruits.aggregate([
    {
        $match:{
            onSale:true
        }
    },
    {
        $group:{
            _id:"$supplier_id",
            total:{
                $sum: "$stock"
            }
        }
    }
]);
/**
 * $project
 *  -can be used when aggregating data to include/exclude fields from the returned result
 * SYNTAX:
 *  {
 *      $project:{
 *          field: true/false
 *      }
 *  }
 */
db.fruits.aggregate([
    {
        $match:{
            onSale:true
        }
    },
    {
        $group:{
            _id:"$supplier_id",
            total:{
                $sum: "$stock"
            }
        }
    },
    {
        $project:{
            _id:0
        }
    }
]);
/**
 * $sort
 *  -used to change the order of aggregated results
 */
db.fruits.aggregate([
    {
        $match:{
            onSale:true
        }
    },
    {
        $group:{
            _id:"$supplier_id",
            total:{
                $sum: "$stock"
            }
        }
    },
    {
        $sort:{
            total:-1
        }
    }
]);
db.fruits.aggregate([
    {
        $unwind:"$origin"
    }
]);
db.fruits.aggregate([
    {
        $unwind:"$origin"
    },
    {
        $group:{
            _id:"$origin",
            kinds:{
                $sum:1
            }
        }
    }
]);

// SECTION - Schema Design
/**
 * -schema design/data modelling is an important feature when creating databases
 * -MongoDB documents can be categorized into normalized or de-normalized/embedded data
 * -Normalized data referes to a data sctructure where documents are reffered to each other using their ids for related pieces of information
 */
var owner = ObjectId();
db.owners.insert(
    {
        _id:owner,
        name:"John Smith",
        contact:"09123456789"
    }
);